package com.secondyear.java.repository;

import com.secondyear.java.model.Tempfiles;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface TempfilesRepository extends CrudRepository<Tempfiles,Long> {

    List<Tempfiles> findAll();
}
