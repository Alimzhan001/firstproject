package com.secondyear.java.repository;

import com.secondyear.java.model.Notification;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface NotificationRepository extends CrudRepository<Notification,Long> {

    List<Notification> findAll();
}
