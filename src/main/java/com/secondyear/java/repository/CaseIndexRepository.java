package com.secondyear.java.repository;

import com.secondyear.java.model.CaseIndex;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CaseIndexRepository extends CrudRepository<CaseIndex,Long> {

    List<CaseIndex> findAll();
}
