package com.secondyear.java.repository;

import com.secondyear.java.controllers.AuthorizationController;
import com.secondyear.java.model.Authorization;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuthorizationRepository extends CrudRepository<Authorization, Long> {


    List<Authorization> findAll();
}
