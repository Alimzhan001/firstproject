package com.secondyear.java.controllers;

import com.secondyear.java.model.FileRouting;
import com.secondyear.java.services.FileRoutingServices;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class FileRoutingController {

    private final FileRoutingServices fileRoutingServices;

    public FileRoutingController(FileRoutingServices fileRoutingServices) {
        this.fileRoutingServices = fileRoutingServices;
    }

    @GetMapping("/FilesRouting")
    public ResponseEntity<?> getFilesRouting(){
        return ResponseEntity.ok ( fileRoutingServices.getAll () );
    }

    @GetMapping("/FileRouting/{fileRoutingId}")
    public ResponseEntity<?> getFileRouting(@PathVariable Long fileRoutingId){
        return ResponseEntity.ok ( fileRoutingServices.getById ( fileRoutingId ) );
    }

    @PostMapping("/FileRouting")
    public ResponseEntity<?> saveFileRouting(@RequestBody FileRouting fileRouting){
        return ResponseEntity.ok ( fileRoutingServices.create ( fileRouting ) );
    }

    @PutMapping("/FileRouting")
    public ResponseEntity<?> updateFileRouting(@RequestBody FileRouting fileRouting){
        return ResponseEntity.ok ( fileRoutingServices.update ( fileRouting ) );
    }

    @DeleteMapping("/FileRouting/fileRoutingId")
    public void deleteFileRouting(@PathVariable Long fileRoutingId){
        fileRoutingServices.delete ( fileRoutingId );
    }
}
