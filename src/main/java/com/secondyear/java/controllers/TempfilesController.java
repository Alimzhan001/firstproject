package com.secondyear.java.controllers;

import com.secondyear.java.model.Tempfiles;
import com.secondyear.java.services.TempfilesServices;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class TempfilesController {

    private final TempfilesServices tempfilesServices;

    public TempfilesController(TempfilesServices tempfilesServices) {
        this.tempfilesServices = tempfilesServices;
    }

    @GetMapping("/Tempfiles")
    public ResponseEntity<?> getTempfiles(){
        return ResponseEntity.ok ( tempfilesServices.getAll () );
    }

    @GetMapping("/Tempfile/{tempfilesId}")
    public ResponseEntity<?> getTempfile(@PathVariable Long tempfilesId){
        return ResponseEntity.ok (tempfilesServices.getById ( tempfilesId ) );
    }

    @PostMapping("/Tempfiles")
    public ResponseEntity<?> saveTempfiles(@RequestBody Tempfiles tempfiles){
        return ResponseEntity.ok ( tempfilesServices.create ( tempfiles ) );
    }

    @PutMapping("/Tempfiles")
    public ResponseEntity<?> updateTempfiles(@RequestBody Tempfiles tempfiles){
        return ResponseEntity.ok ( tempfilesServices.update ( tempfiles ) );
    }

    @DeleteMapping("/Tempfiles/{tempfilesId}")
    public void deleteTempfiles(@PathVariable Long tempfilesId){
        tempfilesServices.delete ( tempfilesId );
    }
}
