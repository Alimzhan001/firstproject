package com.secondyear.java.controllers;

import com.secondyear.java.model.Notification;
import com.secondyear.java.services.NotificationServices;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class NotificationController {

    private final NotificationServices notificationServices;

    public NotificationController(NotificationServices notificationServices) {
        this.notificationServices = notificationServices;
    }

    @GetMapping("/Notifications")
    public ResponseEntity<?> getNotifications(){
        return ResponseEntity.ok ( notificationServices.getAll () );
    }

    @GetMapping("/Notification/{notificationId}")
    public ResponseEntity<?> getNotification(@PathVariable Long notificationId){
        return ResponseEntity.ok ( notificationServices.getById ( notificationId ) );
    }

    @PostMapping("/Notification")
    public ResponseEntity<?> saveNotification(@RequestBody Notification notification){
        return ResponseEntity.ok ( notificationServices.create ( notification ) );
    }

    @PutMapping("/Notification")
    public ResponseEntity<?> updateNotification(@RequestBody Notification notification){
        return ResponseEntity.ok ( notificationServices.update ( notification ) );
    }

    @DeleteMapping("/Notification/{notificationId}")
    public void deleteNotification(@PathVariable Long notificationId){
        notificationServices.delete ( notificationId );
    }

}
