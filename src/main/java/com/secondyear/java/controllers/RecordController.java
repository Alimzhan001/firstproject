package com.secondyear.java.controllers;

import com.secondyear.java.model.Record;
import com.secondyear.java.services.RecordServices;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class RecordController {

    private final RecordServices recordServices;

    public RecordController(RecordServices recordServices) {
        this.recordServices = recordServices;
    }

    @GetMapping("/Records")
    public ResponseEntity<?> getRecords(){
        return ResponseEntity.ok ( recordServices.getAll () );
    }

    @GetMapping("/Record/{recordId}")
    public ResponseEntity<?> getRecord(@PathVariable Long recordId){
        return ResponseEntity.ok ( recordServices.getById ( recordId ) );
    }

    @PostMapping("/Record")
    public ResponseEntity<?> saveRecord(@RequestBody Record record){
        return ResponseEntity.ok ( recordServices.create ( record ) );
    }

    @PutMapping("/Record")
    public ResponseEntity<?> updateRecord(@RequestBody Record record){
        return ResponseEntity.ok ( recordServices.update ( record ) );
    }

    @DeleteMapping("/Record/{recordId}")
    public void deleteRecord(@PathVariable Long recordId){
        recordServices.delete ( recordId );
    }
}
