package com.secondyear.java.controllers;

import com.secondyear.java.model.NomenclatureSummary;
import com.secondyear.java.services.NomenclatureSummaryServices;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class NomenclatureSummaryController {

    private final NomenclatureSummaryServices nomenclatureSummaryServices;

    public NomenclatureSummaryController(NomenclatureSummaryServices nomenclatureSummaryServices) {
        this.nomenclatureSummaryServices = nomenclatureSummaryServices;
    }

    @GetMapping("/NomenclatureSummaries")
    public ResponseEntity<?> getNomenclatureSummaries(){
        return ResponseEntity.ok ( nomenclatureSummaryServices.getAll () );
    }

    @GetMapping("/NomenclatureSummary/{nomenclatureSummaryId}")
    public ResponseEntity<?> getNomenclatureSummary(@PathVariable Long nomenclatureSummaryId){
        return ResponseEntity.ok ( nomenclatureSummaryServices.getById ( nomenclatureSummaryId ) );
    }

    @PostMapping("/NomenclatureSummary")
    public ResponseEntity<?> saveNomenclatureSummary(@RequestBody NomenclatureSummary nomenclatureSummary){
        return ResponseEntity.ok ( nomenclatureSummaryServices.create ( nomenclatureSummary ) );
    }

    @PutMapping("/NomenclatureSummary")
    public ResponseEntity<?> updateNomenclatureSummary(@RequestBody NomenclatureSummary nomenclatureSummary){
        return ResponseEntity.ok ( nomenclatureSummaryServices.update ( nomenclatureSummary ) );
    }

    @DeleteMapping("/NomenclatureSummary/{nomenclatureSummaryId}")
    public void delete(@PathVariable Long nomenclatureSummaryId){
        nomenclatureSummaryServices.delete ( nomenclatureSummaryId );
    }
}
