package com.secondyear.java.controllers;

import com.secondyear.java.model.Case;
import com.secondyear.java.services.CaseServices;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CaseController {

    private final CaseServices caseServices;

    public CaseController(CaseServices caseServices) {
        this.caseServices = caseServices;
    }

    @GetMapping("/Cases")
    public ResponseEntity<?> getCases(){
        return ResponseEntity.ok ( caseServices.getAll () );
    }

    @GetMapping("Case/{caseId}")
    public  ResponseEntity<?> getCase(@PathVariable Long caseId){
        return ResponseEntity.ok ( caseServices.getById ( caseId ) );
    }

    @PostMapping("/Case")
    public ResponseEntity<?> saveCase(@RequestBody Case cases){
        return ResponseEntity.ok ( caseServices.create ( cases ) );
    }

    @PutMapping("/Case")
    public ResponseEntity<?> updateCase(@RequestBody Case cases){
        return ResponseEntity.ok ( caseServices.update ( cases ) );
    }

    @DeleteMapping("/Case/{caseId}")
    public void deleteCase(@PathVariable Long caseId){
        caseServices.delete ( caseId );
    }
}
