package com.secondyear.java.controllers;

import com.secondyear.java.model.SearchKey;
import com.secondyear.java.services.SearchKeyServices;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class SearchKeyController {

    private final SearchKeyServices searchKeyServices;

    public SearchKeyController(SearchKeyServices searchKeyServices) {
        this.searchKeyServices = searchKeyServices;
    }

    @GetMapping("/SearchKeys")
    public ResponseEntity<?> getSearchKeys(){
        return ResponseEntity.ok ( searchKeyServices.getAll () );
    }

    @GetMapping("/SearchKey/{searchKeyId}")
    public ResponseEntity<?> getSearchKey(@PathVariable Long searchKeyId){
        return ResponseEntity.ok ( searchKeyServices.getById ( searchKeyId ) );
    }

    @PostMapping("/SearchKey")
    public ResponseEntity<?> saveSearchKey(@RequestBody SearchKey searchKey){
        return ResponseEntity.ok ( searchKeyServices.create ( searchKey ) );
    }

    @PutMapping("/SearchKey")
    public ResponseEntity<?> updateSearchKey(@RequestBody SearchKey searchKey){
        return ResponseEntity.ok ( searchKeyServices.update ( searchKey ) );
    }

    @DeleteMapping("/SearchKey/{searchKeyId}")
    public void deleteSearchKey(@PathVariable Long searchKeyId){
        searchKeyServices.delete ( searchKeyId );
    }
}
